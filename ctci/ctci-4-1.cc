#include <initializer_list>
#include <utility>
#include <string>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <deque>
#include <stdexcept>
#include <iostream>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#define S(a) std::string{a}
#define L(...) std::initializer_list<std::string>{__VA_ARGS__}
#define P(a, b) std::make_pair(a, b)

namespace {
  class Graph {
   private:
    struct Node {
      std::string name;
      std::unordered_set<std::string> neighbors;
      Node(const std::string& nm, const std::initializer_list<std::string>& nbrs)
        : name(nm), neighbors(nbrs)
      {}
      bool visited = false;
    };

    std::unordered_map<std::string, std::shared_ptr<Node>> vertices;

    void ResetVisited() const {
      for (auto& v : vertices) {
        v.second->visited = false;
      }
    }

    /*
    void RemoveVisited(std::unordered_set<std::string>& full) {
      for (auto i = full.cbegin(); i != full.cend();) {
        if (vertices.at(*i)->visited) {
          i = full.erase(i);
        } else {
          ++i;
        }
      }
    }
    */

    bool BreadthFirstSearch(const std::string& src, const std::string& tgt) const {
      ResetVisited();
      std::deque<std::string> tovisit{src};
      while (!tovisit.empty()) {
        auto nv = tovisit.front();
        tovisit.pop_front();
        auto i = vertices.at(nv);
        if (i->visited) {
          continue;
        }

        i->visited = true;
        auto ng = i->neighbors;
        if (ng.count(tgt)) {
          return true;
        }
        tovisit.insert(tovisit.end(), ng.cbegin(), ng.cend());
      }

      return false;
    }

   public:
    Graph(const std::initializer_list<std::pair<std::string, std::initializer_list<std::string>>>& adj) {
      for (auto& p : adj) {
        vertices[p.first] = std::make_shared<Node>(p.first, p.second);
      }
    }

    bool HasPath(const std::string& src, const std::string& tgt) {
      return BreadthFirstSearch(src, tgt);
    }
  };

  TEST_CASE("Connected graph with few connected nodes. Must be able to traverse from one vertex to another.") {
    Graph g{{
      P(S("a"), L(S("b"), S("c")))
    }, {
      P(S("b"), L())
    }, {
      P(S("c"), L())
    }};
    CHECK(g.HasPath("a", "b"));
  }

  TEST_CASE("Transitive nature of graph: a -> b -> c => a -> c.") {
    Graph g{{
      P(S("a"), L(S("b"), S("d")))
    }, {
      P(S("b"), L(S("c")))
    }, {
      P(S("c"), L())
    }, {
      P(S("d"), L())
    }};
    CHECK(g.HasPath("a", "c"));
  }

  TEST_CASE("Parallel nature of graph: a -> b -> d -> e and a -> d -> e => a -> e.") {
    Graph g{{
      P(S("a"), L(S("b"), S("d")))
    }, {
      P(S("b"), L(S("c"), S("d")))
    }, {
      P(S("c"), L())
    }, {
      P(S("d"), L(S("e"), S("f")))
    }, {
      P(S("e"), L())
    }, {
      P(S("f"), L())
    }};
    CHECK(g.HasPath("a", "e"));
  }

  TEST_CASE("Disconnected Graph.") {
    Graph g{{
      P(S("a"), L(S("b"), S("d")))
    }, {
      P(S("b"), L(S("c"), S("d")))
    }, {
      P(S("c"), L())
    }, {
      P(S("d"), L(S("e")))
    }, {
      P(S("e"), L())
    }, {
      P(S("f"), L())
    }};
    CHECK(!g.HasPath("a", "f"));
  }

  TEST_CASE("Connected graph with cycles with no path between a specific pair of vertices.") {
    Graph g{{
      P(S("a"), L(S("b"), S("d")))
    }, {
      P(S("b"), L(S("c"), S("d")))
    }, {
      P(S("c"), L())
    }, {
      P(S("d"), L(S("e")))
    }, {
      P(S("e"), L(S("a")))
    }, {
      P(S("f"), L())
    }};
    CHECK(!g.HasPath("a", "f"));
  }

  TEST_CASE("Connected graph with cycles should have a path from any vertex in the cycle back to the same vertex.") {
    Graph g{{
      P(S("a"), L(S("b"), S("d")))
    }, {
      P(S("b"), L(S("c"), S("d")))
    }, {
      P(S("c"), L())
    }, {
      P(S("d"), L(S("e")))
    }, {
      P(S("e"), L(S("a")))
    }, {
      P(S("f"), L())
    }};
    CHECK(g.HasPath("a", "a"));
    CHECK(g.HasPath("d", "d"));
  }

  TEST_CASE("Connected acyclic graph shouldn't have a path from a specific vertex back to itself.") {
    Graph g{{
      P(S("a"), L(S("b"), S("d")))
    }, {
      P(S("b"), L(S("c"), S("d")))
    }, {
      P(S("c"), L())
    }, {
      P(S("d"), L(S("e")))
    }, {
      P(S("e"), L(S("f")))
    }, {
      P(S("f"), L())
    }};
    CHECK(!g.HasPath("a", "a"));
  }
}

