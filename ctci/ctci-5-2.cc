/*
 * Haribol!
 * Problem: Given a real umber between 0 and 1 (e.g., 0.72) that is passed in as a double, return the binary representation. If the number cannot be represented accurately in binary with at most 32 characters, let the caller know.
 */

#include <string>
#include <utility>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  constexpr double kFloatEpsilon = 1e-10;

  std::pair<std::string, bool> tostring(double f) {
    if (f < 0 || f > 1) {
      throw std::invalid_argument("Input argument must lie between 0 and 1.");
    }

    std::string r{"0."};

    while (r.size() < 34) {
      f *= 2;
      const auto s = f >= 1 ? 1 : 0;
      r.append(s == 1 ? "1" : "0");
      f -= s;
      if (f <= kFloatEpsilon) break;
    }

    return std::make_pair(r, f <= kFloatEpsilon);
  }

  TEST_CASE("Convert double (0.5) to binary string representation.") {
    auto r = tostring(0.5);
    CHECK(r.first.substr(0, 3) == "0.1");
    CHECK(r.first.size() == 3);
    CHECK(r.second);
  }

  TEST_CASE("Convert double (greater than 0.5) to binary string representation.") {
    auto r = tostring(0.76);
    CHECK(r.first.substr(0, 9) == "0.1100001");
    CHECK(!r.second);
  }

  TEST_CASE("Convert double (lesser than 0.5) to binary string representation.") {
    auto r = tostring(0.3);
    CHECK(r.first.substr(0, 7) == "0.01001");
    CHECK(!r.second);
  }
}
