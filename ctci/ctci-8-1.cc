/*
 * Haribol!
 * Problem: A child is running up a staircase with n steps and can hop either 1 step, 2 steps, or 3 steps at a time. Implement a method to count how many possible ways the child can run up the stairs.
 */

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  unsigned EnumerateWays(unsigned n) {
    if (n == 0) {
      throw std::invalid_argument("n must be greater than zero!");
    }
    if (n <= 3) {
      return n;
    }
    unsigned fn3 = 1, fn2 = 2, fn1 = 4;
    unsigned fn;
    for (unsigned i = 4; i <= n; ++i) {
      fn = fn1 + fn2 + fn3;
      fn3 = fn2;
      fn2 = fn1;
      fn1 = fn;
    }

    return fn;
  }

  TEST_CASE("n = 4.") {
    CHECK(EnumerateWays(4) == 7);
  }

  TEST_CASE("n = 5.") {
    CHECK(EnumerateWays(5) == 13);
  }

  TEST_CASE("n = 6.") {
    CHECK(EnumerateWays(6) == 24);
  }
}

