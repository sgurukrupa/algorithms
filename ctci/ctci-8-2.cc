/*
 * Haribol!
 * Problem: ctci 8.2
 * Imagine a robot sitting on the upper left corner of grid with r rows and c columns. The robot can only move in two directions, right and down, but certain cells are "off limits" such that the robot cannot step on them. Design an algorithm to find a path for the robot from the top left to the bottom right.
 */

#include <vector>
#include <utility>
#include <deque>
#include <cassert>
#include <algorithm>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  struct Node {
    enum Parent { kNone = 0, kUp, kLeft, kMine };
    Parent parent;
  };

  class Grid {
   public:
    struct Coordinate {
      unsigned row, col;

      bool operator ==(const Coordinate& other) const {
        return row == other.row && col == other.col;
      }
      bool operator !=(const Coordinate& other) const {
        return !operator ==(other);
      }
    };

    struct Path {
      Coordinate coords;
      Node::Parent parent;
    };

   private:
    unsigned rows_, cols_;
    std::vector<Node> path_;

    unsigned Index(const Coordinate& c) {
      return c.row * cols_ + c.col; // grid is row-major, Haribol
    }

    bool IsMine(const Coordinate& c) {
      return path_[Index(c)].parent == Node::kMine;
    }

    bool IsVisited(const Coordinate& c) {
      const auto p = path_[Index(c)].parent;
      return !(p == Node::kNone || p == Node::kMine);
    }

    void ComputePath() {
      std::deque<Path> tovisit{Path{}};

      while (!tovisit.empty()) {
        auto tp = tovisit.front();
        tovisit.pop_front();
        auto& c = tp.coords;
        if (IsVisited(c)) {
          continue;
        }
        path_[Index(c)].parent = tp.parent; // this also => visited, Haribol

        ++c.row;
        if (c.row < rows_ && !IsMine(c) && !IsVisited(c)) {
          tp.parent = Node::kUp;
          if (c == Coordinate{rows_ - 1, cols_ - 1}) {
            path_[Index(c)].parent = tp.parent;
            break; // reached destination
          }
          tovisit.push_back(tp);
        }

        --c.row;
        ++c.col;
        if (c.col < cols_ && !IsMine(c) && !IsVisited(c)) {
          tp.parent = Node::kLeft;
          if (c == Coordinate{rows_ - 1, cols_ - 1}) {
            path_[Index(c)].parent = tp.parent;
            break; // reached destination
          }
          tovisit.push_back(tp);
        }
      }
    }

   public:
    Grid(unsigned r, unsigned c, const std::initializer_list<Coordinate>& mines)
      : rows_{r}, cols_{c}, path_{r * c, Node{}} {
      for (auto& m : mines) {
        path_[Index(m)].parent = Node::kMine;
      }
    }

    Grid(const std::initializer_list<bool>& cells, unsigned c)
      : rows_(cells.size() / c), cols_{c} {
      path_.reserve(cells.size());
      for (auto b : cells) {
        path_.push_back(Node{b ? Node::kMine : Node::kNone});
      }
    }

    std::vector<Coordinate> FindPath() {
      ComputePath();
      std::vector<Coordinate> p;
      for (Coordinate c{rows_ - 1, cols_ - 1}; c != Coordinate{0, 0};) {
        auto parent = path_[Index(c)].parent;
        if (parent != Node::kNone) {
          p.push_back(c);
        } else {
          break;
        }
        switch (parent) {
          case Node::kUp:
            --c.row;
            break;
          case Node::kLeft:
            --c.col;
            break;
          default:
            assert("Got a kMine!" && false);
        }
      }
      p.push_back(Coordinate{0, 0});
      std::reverse(p.begin(), p.end());
      return p;
    }
  };

  TEST_CASE("3 X 3 grid - two mines") {
    Grid g{{
      0, 1, 0,
      0, 1, 0,
      0, 0, 0}, 3};
    auto p = g.FindPath();
    CHECK(p.size() == 5);
    CHECK(p[0] == Grid::Coordinate{0, 0});
    CHECK(p[1] == Grid::Coordinate{1, 0});
    CHECK(p[2] == Grid::Coordinate{2, 0});
    CHECK(p[3] == Grid::Coordinate{2, 1});
    CHECK(p[4] == Grid::Coordinate{2, 2});
  }

  TEST_CASE("3 X 3 grid - two mines") {
    Grid g{{
      0, 0, 0,
      0, 1, 0,
      1, 0, 0}, 3};
    auto p = g.FindPath();
    CHECK(p.size() == 5);
    CHECK(p[0] == Grid::Coordinate{0, 0});
    CHECK(p[1] == Grid::Coordinate{0, 1});
    CHECK(p[2] == Grid::Coordinate{0, 2});
    CHECK(p[3] == Grid::Coordinate{1, 2});
    CHECK(p[4] == Grid::Coordinate{2, 2});
  }

  TEST_CASE("3 X 3 grid - two mines") {
    Grid g{{
      0, 1, 0,
      0, 0, 0,
      1, 0, 0}, 3};
    auto p = g.FindPath();
    CHECK(p.size() == 5);
    CHECK(p[0] == Grid::Coordinate{0, 0});
    CHECK(p[1] == Grid::Coordinate{1, 0});
    CHECK(p[2] == Grid::Coordinate{1, 1});
    CHECK(p[3] == Grid::Coordinate{2, 1});
    CHECK(p[4] == Grid::Coordinate{2, 2});
  }

  TEST_CASE("3 X 3 grid - single mine near finishing point") {
    Grid g{{
      0, 0, 0,
      0, 0, 0,
      0, 1, 0}, 3};
    auto p = g.FindPath();
    CHECK(p.size() == 5);
    CHECK(p[0] == Grid::Coordinate{0, 0});
    CHECK(p[1] == Grid::Coordinate{1, 0});
    CHECK(p[2] == Grid::Coordinate{1, 1});
    CHECK(p[3] == Grid::Coordinate{1, 2});
    CHECK(p[4] == Grid::Coordinate{2, 2});
  }

  TEST_CASE("3 X 3 grid - single mine near finishing point") {
    Grid g{{
      0, 0, 0,
      0, 0, 1,
      0, 0, 0}, 3};
    auto p = g.FindPath();
    CHECK(p.size() == 5);
    CHECK(p[0] == Grid::Coordinate{0, 0});
    CHECK(p[1] == Grid::Coordinate{1, 0});
    CHECK(p[2] == Grid::Coordinate{2, 0});
    CHECK(p[3] == Grid::Coordinate{2, 1});
    CHECK(p[4] == Grid::Coordinate{2, 2});
  }

  TEST_CASE("3 X 3 grid - single mine near starting point") {
    Grid g{{
      0, 0, 0,
      1, 0, 0,
      0, 0, 0}, 3};
    auto p = g.FindPath();
    CHECK(p.size() == 5);
    CHECK(p[0] == Grid::Coordinate{0, 0});
    CHECK(p[1] == Grid::Coordinate{0, 1});
    CHECK(p[2] == Grid::Coordinate{1, 1});
    CHECK(p[3] == Grid::Coordinate{2, 1});
    CHECK(p[4] == Grid::Coordinate{2, 2});
  }

  TEST_CASE("3 X 3 grid - single mine near starting point") {
    Grid g{{
      0, 1, 0,
      0, 0, 0,
      0, 0, 0}, 3};
    auto p = g.FindPath();
    CHECK(p.size() == 5);
    CHECK(p[0] == Grid::Coordinate{0, 0});
    CHECK(p[1] == Grid::Coordinate{1, 0});
    CHECK(p[2] == Grid::Coordinate{2, 0});
    CHECK(p[3] == Grid::Coordinate{2, 1});
    CHECK(p[4] == Grid::Coordinate{2, 2});
  }

  TEST_CASE("3 X 3 grid - single central mine") {
    Grid g{{
      0, 0, 0,
      0, 1, 0,
      0, 0, 0}, 3};
    auto p = g.FindPath();
    CHECK(p.size() == 5);
    CHECK(p[0] == Grid::Coordinate{0, 0});
    CHECK(p[1] == Grid::Coordinate{1, 0});
    CHECK(p[2] == Grid::Coordinate{2, 0});
    CHECK(p[3] == Grid::Coordinate{2, 1});
    CHECK(p[4] == Grid::Coordinate{2, 2});
  }

  TEST_CASE("3 X 3 grid - no path") {
    Grid g{{
      0, 0, 0,
      0, 0, 1,
      0, 1, 0}, 3};
    auto p = g.FindPath();
    CHECK(p.size() == 1);
    CHECK(p[0] == Grid::Coordinate{0, 0});
  }

  TEST_CASE("4 X 4 grid") {
    Grid g{{
      0, 0, 0, 0,
      1, 0, 0, 0,
      0, 0, 1, 0,
      0, 1, 0, 0
    }, 4};
    auto p = g.FindPath();
    CHECK(p.size() == 7);
    CHECK(p[0] == Grid::Coordinate{0, 0});
    CHECK(p[1] == Grid::Coordinate{0, 1});
    CHECK(p[2] == Grid::Coordinate{1, 1});
    CHECK(p[3] == Grid::Coordinate{1, 2});
    CHECK(p[4] == Grid::Coordinate{1, 3});
    CHECK(p[5] == Grid::Coordinate{2, 3});
    CHECK(p[6] == Grid::Coordinate{3, 3});
  }

  TEST_CASE("4 X 4 grid") {
    Grid g{{
      0, 0, 0, 0,
      1, 0, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 0
    }, 4};
    auto p = g.FindPath();
    CHECK(p.size() == 7);
    CHECK(p[0] == Grid::Coordinate{0, 0});
    CHECK(p[1] == Grid::Coordinate{0, 1});
    CHECK(p[2] == Grid::Coordinate{1, 1});
    CHECK(p[3] == Grid::Coordinate{2, 1});
    CHECK(p[4] == Grid::Coordinate{3, 1});
    CHECK(p[5] == Grid::Coordinate{3, 2});
    CHECK(p[6] == Grid::Coordinate{3, 3});
  }
}

