/*
 * Haribol!
 * Problem: ctci 8.7 - Permutations without Dups
 */

#include <string>
#include <iostream>

namespace {
  // Wow! got this logic right by Lord Krishna's mercy
  // second argument can be passed by value too
  void Select(std::string a, std::string& selected) {
    if (a.empty()) {
      std::cout << selected << std::endl;
    }

    for (std::string::size_type i = 0; i < a.size(); ++i) {
      selected.push_back(a[i]);
      Select(a.substr(0, i) + a.substr(i + 1), selected);
      selected.pop_back();
    }
  }
}

int main() {
  std::string s;
  Select("abcd", s);
}
