/*
 * Haribol!
 * Problem: ctci 5.1
 */

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  int Merge(int m, int n, const int i, const int j) {
    const int numbits = j - i + 1;
    unsigned mask = (static_cast<unsigned>(1) << numbits) - 1;
    m &= mask;
    n &= ~(mask << i);
    return n | (m << i);
  }

  TEST_CASE("Insert an int into another int.") {
    auto r = Merge(0x1a, 0xb6, 1, 5);
    CHECK(r == 0xb4);
  }

  TEST_CASE("Insert an int into another int. mask contains more bits than m") {
    auto r = Merge(0x1a, 0xb6, 1, 7);
    CHECK(r == 0x34);
  }

  TEST_CASE("Insert an int into another int. mask extends beyond n") {
    auto r = Merge(0x1a, 0xb6, 6, 13);
    CHECK(r == 0x6b6);
  }
}
