/*
 * Haribol!
 * Problem: ctci 8.6 - Towers of Hanoi
 */

#include <array>
#include <cstddef>
#include <stack>
#include <cassert>
#include <iostream>

namespace {
  using Disk = unsigned; // series of positive integers whose values signify the size

  void Print(const std::stack<Disk>& tower) {
    struct Stack : std::stack<Disk> {
      using std::stack<Disk>::c;
    };

    static_assert(sizeof(Stack) == sizeof(std::stack<Disk>), "Unequal sizes!");
    static_assert(alignof(Stack) == alignof(std::stack<Disk>), "Alignment mismatch!");
    auto& c = static_cast<const Stack&>(tower).c;
    for (auto i = c.cbegin(); i != c.cend(); ++i) {
      std::cout << *i << ' ';
    }
    std::cout << std::endl;
  }

  // Solved this by Lord Krishna's mercy, Haribol
  void SolveToh(const unsigned n, const std::size_t srcIx, const std::size_t tgtIx, std::array<std::stack<Disk>, 3>& towers) {
    const std::size_t empIx =
      tgtIx == 0 && srcIx == 1 ||
      tgtIx == 1 && srcIx == 0 ? 2 :
      tgtIx == 0 && srcIx == 2 ||
      tgtIx == 2 && srcIx == 0 ? 1 : 0;

    if (n > 1) {
      SolveToh(n - 1, srcIx, empIx, towers);
    }

    auto& srcTower = towers[srcIx];
    auto d = srcTower.top(); // the largest disk
    srcTower.pop();
    auto& tgtTower = towers[tgtIx];
    assert((tgtTower.empty() || d < tgtTower.top()));
    tgtTower.push(d);

    std::cout << "tower# " << srcIx << " -> " << tgtIx << ": ";
    Print(tgtTower);

    if (n > 1) {
      SolveToh(n - 1, empIx, tgtIx, towers);
    }
  }
}

int main() {
  std::array<std::stack<Disk>, 3> towers;
  auto& src = towers[0];
  src.push(4);
  src.push(3);
  src.push(2);
  src.push(1);
  SolveToh(4, 0, 2, towers);
}

