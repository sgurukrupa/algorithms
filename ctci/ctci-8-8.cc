/*
 * Haribol!
 * Problem: ctci 8.7 - Permutations with Dups
 */

#include <string>
#include <iostream>
#include <algorithm>

namespace {
  void Select(std::string a, std::string& selected) {
    if (a.empty()) {
      std::cout << selected << std::endl;
    }

    for (std::string::size_type i = 0; i < a.size(); ++i) {
      if (i > 0 && a[i] == a[i-1]) {
        continue;
      }
      selected.push_back(a[i]);
      Select(a.substr(0, i) + a.substr(i + 1), selected);
      selected.pop_back();
    }
  }
}

int main() {
  std::string a;
  std::cout << "Enter a string to permute: ";
  std::cin >> a;
  std::sort(a.begin(), a.end());

  std::string s;
  Select(a, s);
}
