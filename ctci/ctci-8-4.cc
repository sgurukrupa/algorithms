/*
 * Haribol!
 * Problem: Write a method to return all subsets of a set.
 */

#include <unordered_set>
#include <vector>
#include <iostream>

namespace {
  void Print(const std::vector<int>& p) {
    std::cout << "{ ";
    if (!p.empty()) {
      std::cout << p[p.size() - 1];
      for (auto i = p.size() - 1; i > 0; --i) {
        std::cout << ", " << p[i - 1];
      }
    }
    std::cout << " }" << std::endl;
  }

  // Got the following logic right by Lord Krishna's mercy!!
  // the second argument can be passed by value too, Haribol
  void Select(std::vector<int>& a, std::vector<int>& selected) {
    if (a.empty()) {
      Print(selected);
      return; // ends recursion, Haribol!
    }

    auto elem = a.back();
    a.pop_back();

    selected.push_back(elem);
    Select(a, selected);

    selected.pop_back();
    Select(a, selected);

    a.push_back(elem); // restore original, Haribol!
  }
}

int main() {
  std::vector<int> a = {1, 2, 3, 4};
  std::vector<int> s;
  Select(a, s);
}

