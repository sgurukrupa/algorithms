/*
 * Haribol!
 * Problem: Given a positive integer, print the next (previous) smallest and the next largest number that have the same number of 1 bits in their binary representation.
 */

#include <type_traits>
#include <iostream>
#include <cstddef>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  unsigned GetPrevious(unsigned m) {
    // find the least significant 1 and push it to right by one position
    // if the rightmost bit is a 1, get the next most significant bit and do the same
    auto n = m;
    std::size_t i = 0; // number of 1s before 0
    std::size_t j = 0; // count 0s
    while (n > 0) {
      if (!(n & 1)) {
        ++j;
      } else {
        if (j > 0) {
          break;
        } else {
          ++i;
        }
      }
      n >>= 1;
    }

    if (m == 0) {
      throw std::invalid_argument("No number lesser than 0.");
    } else if (j == 0) {
      throw std::invalid_argument("No number lesser than the input number.");
    }

    unsigned mask = static_cast<unsigned>(3) << (i + j - 1); // 3 => 11 in binary
    return m ^ mask;
  }

  template <typename T>
  std::vector<bool> BitPattern(T m) {
    static_assert(std::is_integral_v<decltype(m)> && std::is_unsigned_v<decltype(m)>, "Only unsigned integral types can be used as input.");
    std::vector<bool> bits;
    while (m != 0) {
      bits.push_back(m & 1);
      m >>= 1;
    }

    return bits.empty() ? std::vector<bool>{} : bits;
  }

  unsigned GetNext(unsigned m) {
    // find the least significant 1 and all 1s before it
    // if there's a zero before it - replace it with the following 1
    // push all the remaining ones to the right
    constexpr auto kNil = static_cast<std::size_t>(-1);

    auto n = m;

    std::size_t j; // first 1 from the right, Haribol
    std::size_t i;

    for (j = kNil, i = 0; n != 0; n >>= 1, ++i) {
      if ((n & 1) && j == kNil) {
        j = i;
      } else if (!(n & 1) && (j != kNil)) {
        break;
      }
    }

    if (j == kNil) {
      throw std::invalid_argument("No next higher number for 0!");
    } else if (i == 31) {
      throw std::invalid_argument("No next higher number.");
    }

    // we require i - j - 1 number of 1s to be pushed right, Haribol
    // we require ith bit to be flipped
    unsigned mask = 1 << i;
    m ^= mask; // flip ith bit
    m &= static_cast<decltype(m)>(-1) << i; // clear all bits lower than i
    m |= ~(static_cast<decltype(m)>(-1) << (i - j - 1));
    return m;
  }

  TEST_CASE("m: 1100110") {
    CHECK(GetNext(0x66) == 0x69);
    CHECK(GetPrevious(0x66) == 0x65);
  }

  TEST_CASE("m: 11010") {
    CHECK(GetNext(0x1a) == 0x1c);
    CHECK(GetPrevious(0x1a) == 0x19);
  }

  TEST_CASE("m: 11-0110-0111-1100") {
    CHECK(GetNext(0x367c) == 0x368f);
    CHECK(GetPrevious(0x367c) == 0x367a);
  }

  TEST_CASE("m: 1101") {
    CHECK(GetNext(0xd) == 0xe);
    CHECK(GetPrevious(0xd) == 0xb);
  }

  TEST_CASE("m: 110100") {
    CHECK(GetNext(0x34) == 0x38);
    CHECK(GetPrevious(0x34) == 0x32);
  }
}
