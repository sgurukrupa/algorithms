/*
 * Haribol!
 * Problem: You have an integer and you can flip exactly one bit from a 0 to a 1. Write code to find the length of the longest sequence of 1s you could create.
 * Example:
 * Input: 1775 (or: 11011101111)
 * Output: 8
 */
#include <string>
#include <cassert>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
#ifdef LS1
  int LongestSequence(unsigned m) {
    // count the 1s
    // if there's a break (because of a 0), never mind - count the 0 too
    // if there's more than one consecutive 0s note the previous count and restart fresh
    // thus we will have to maintain a maximum of previous counts and that will give the solution
    constexpr auto kMaxOnes = static_cast<int>(CHAR_BIT * sizeof(unsigned));

    int max = 0;
    int count = 0;
    bool gotOne = false;
    bool gotZero = false;
    bool zeroOne = false;
    bool zeroZero = false;
    bool midZero = false;
    while (m != 0) {
      if (m & 1) {
        if (zeroZero) { // the last bit sequence: 00
          max = std::max(max, count) + (midZero ? 0 : 1);
          count = 0;
          midZero = false;
        } else if (zeroOne) { // the last bit sequence: 01
          if (!midZero) {
            ++count; // consider the previous 0 as 1
            midZero = true;
          } else { // we cannot consider more than a single 0 as 1
            max = std::max(max, count);
            count = 2;
          }
        }
        ++count;
        gotZero = zeroZero = zeroOne = false;
        gotOne = true;
      } else {
        if (gotZero) { // last bit was a zero and this one too is a zero
          zeroZero = true;
          zeroOne = false;
        } else if (gotOne) { // last bit wasn't a zero
          zeroZero = false;
          zeroOne = true;
        }
        gotOne = false;
        gotZero = true;
      }
      m >>= 1;
    }

    max = std::max(max, count) + (midZero ? 0 : 1);

    if (max > kMaxOnes) {
      max = kMaxOnes;
    }

    return max;
  }
#endif

#ifdef LS2
  struct Node {
    unsigned bit;
    int size;
  };

  std::vector<Node> CountBits(unsigned m) {
    std::vector<Node> counts;
    unsigned prevBit;
    for (; m != 0; m >>= 1) {
      if (counts.empty() || (m & 1) != prevBit) {
        counts.push_back({prevBit = m & 1, 0});
      }
      ++counts.back().size;
    }

    return counts;
  }

  int LongestSequence(unsigned m) {
    const auto counts = CountBits(m);
    int max = 0;

    for (decltype(CountBits(0))::size_type i = 0; i < counts.size(); ++i) {
      if (counts[i].bit == 1) {
        auto len = counts[i].size;
        if (i > 0 && counts[i - 1].size == 1) {
          assert(("Unexpected previous bit: 1", counts[i - 1].bit == 0));
          ++len;
          if (i > 1) {
            assert(("Unexpected previous bit: 0", counts[i - 2].bit == 1));
            len += counts[i - 2].size;
          }
        } else if (counts[i].size < static_cast<int>(CHAR_BIT * sizeof(unsigned))) {
          // A zero bit can always be counted as a 1
          ++len;
        }

        if (max < len) {
          max = len;
        }
      }
    }

    if (max == 0) {
      max = 1; // there's always a zero bit that can be counted as a 1
    }

    return max;
  }
#endif

  TEST_CASE("m: max unsigned") {
    CHECK(LongestSequence(0xffffffff) == 32);
  }

  TEST_CASE("m: 1") {
    CHECK(LongestSequence(1) == 2);
  }

  TEST_CASE("m: 11010") {
    CHECK(LongestSequence(0x1a) == 4);
  }

  TEST_CASE("m: 1101010") {
    CHECK(LongestSequence(0x1a) == 4);
  }

  TEST_CASE("m: 11011101111") {
    CHECK(LongestSequence(0x6ef) == 8);
  }

  TEST_CASE("m: 110101") {
    CHECK(LongestSequence(0x35) == 4);
  }

  TEST_CASE("m: 11010101") {
    CHECK(LongestSequence(0xd5) == 4);
  }
}
