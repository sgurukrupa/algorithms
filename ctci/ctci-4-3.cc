/*
 * Haribol
 * Problem: Given a binary tree, design an algorithm which creates a linked list of all nodes at each depth (e.g., if you have a tree with depth D, you'll have D linked lists)
 */

#include <memory>
#include <utility>
#include <forward_list>
#include <cstddef>
#include <vector>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  struct Node {
    int value;
    std::shared_ptr<Node> left, right;
    Node(int x) : value(x) {}
  };

  inline std::shared_ptr<Node> MN(int x) {
    return std::make_shared<Node>(x);
  }

  /*
  class LN {
   private:
    std::shared_ptr<Node> node;

   public:
    operator std::shared_ptr<Node>() {
      return std::move(node);
    }

    LN(std::shared_ptr<Node>&& nd) : node(std::move(nd)) {}

    LN(int x) : node(MN(x)) {}
  };
  */

  inline std::shared_ptr<Node> MN(std::shared_ptr<Node>&& left, int x, std::shared_ptr<Node>&& right) {
    auto n = MN(x);
    n->left = std::move(left);
    n->right = std::move(right);
    return n;
  }

  inline std::shared_ptr<Node> MN(int xf, int x, std::shared_ptr<Node>&& right) {
    auto n = MN(x);
    n->left = MN(xf);
    n->right = std::move(right);
    return n;
  }

  inline std::shared_ptr<Node> MN(std::shared_ptr<Node>&& left, int x, int xr) {
    auto n = MN(x);
    n->left = std::move(left);
    n->right = MN(xr);
    return n;
  }

  inline std::shared_ptr<Node> MN(int xf, int x, int xr) {
    auto n = MN(x);
    n->left = MN(xf);
    n->right = MN(xr);
    return n;
  }

  /*
  inline std::shared_ptr<Node> MN(nullptr_t, int x, int xr) {
    auto n = MN(x);
    n->right = std::move(MN(xr));
    return n;
  }

  inline std::shared_ptr<Node> MN(int xf, int x, nullptr_t) {
    auto n = MN(x);
    n->left = std::move(MN(xf));
    return n;
  }
  */

  inline std::shared_ptr<Node> MN(std::shared_ptr<Node>&& left, int x) {
    auto n = MN(x);
    n->left = std::move(left);
    return n;
  }

  inline std::shared_ptr<Node> MN(int x, std::shared_ptr<Node>&& right) {
    auto n = MN(x);
    n->right = std::move(right);
    return n;
  }

  /*
  inline std::shared_ptr<Node> MN(LN&& left, int x) {
    auto n = MN(x);
    n->left = std::move(left);
    return n;
  }

  inline std::shared_ptr<Node> MNL(int xf, int x) {
    auto n = MN(x);
    n->left = std::move(MN(xf));
    return n;
  }

  inline std::shared_ptr<Node> MNR(int x, int xr) {
    auto n = MN(x);
    n->right = std::move(MN(xr));
    return n;
  }
  */

  class BinaryTree {
   private:
    std::shared_ptr<Node> root;

    void EnlistNodes(const std::shared_ptr<Node> node, std::size_t depth,
        std::vector<std::forward_list<std::shared_ptr<Node>>>& lists) {
      if (depth == lists.size()) {
        lists.push_back(std::forward_list<std::shared_ptr<Node>>{});
      }
      lists[depth].push_front(node);
      if (node->right) {
        EnlistNodes(node->right, depth + 1, lists);
      }
      if (node->left) {
        EnlistNodes(node->left, depth + 1, lists);
      }
    }

   public:
    BinaryTree(std::shared_ptr<Node>&& rt) : root(std::move(rt)) {}

    std::vector<std::forward_list<std::shared_ptr<Node>>> EnlistNodes() {
      std::vector<std::forward_list<std::shared_ptr<Node>>> lists;
      EnlistNodes(root, 0, lists);
      return lists;
    }
  };

  TEST_CASE("Construct BinaryTree.") {
    BinaryTree t{
      MN(MN(1, 2, 5), 2, MN(nullptr, 2, MN(4, 5, nullptr)))
    };

    auto s = t.EnlistNodes();
    CHECK(s.size() == 4); {
      auto i = s[0].cbegin();
      CHECK(i != s[0].cend());
      CHECK((*i++)->value == 2);
      CHECK(i == s[0].cend());
    } {
      auto i = s[1].cbegin();
      CHECK(i != s[1].cend());
      CHECK((*i++)->value == 2);
      CHECK(i != s[1].cend());
      CHECK((*i++)->value == 2);
      CHECK(i == s[1].cend());
    } {
      auto i = s[2].cbegin();
      CHECK(i != s[2].cend());
      CHECK((*i++)->value == 1);
      CHECK(i != s[2].cend());
      CHECK((*i++)->value == 5);
      CHECK(i != s[2].cend());
      CHECK((*i++)->value == 5);
      CHECK(i == s[2].cend());
    } {
      auto i = s[3].cbegin();
      CHECK(i != s[3].cend());
      CHECK((*i++)->value == 4);
      CHECK(i == s[3].cend());
    } 
  }
}

