#include <memory>
#include <vector>
#include <algorithm>
#include <stdexcept>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

/*
 * Haribol!
 * Problem: Given a sorted (increasing order) array with unique integer elements, write an algorithm to create a binary search tree with minimal height.
 */

namespace {
  class BinarySearchTree {
   private:
    struct Node {
      int value;
      std::unique_ptr<Node> left, right;
      Node(int v) : value(v) {}
    };

    std::unique_ptr<Node> root;

    using Index = std::vector<int>::size_type;

    void InsertNode(std::unique_ptr<Node>& p, const std::vector<int>& data, const Index low, const Index high) {
      if (low == high) { // empty sequence
        return;
      }

      auto mid = (low + high) / 2;
      p = std::make_unique<Node>(data[mid]);
      InsertNode(p->left, data, low, mid);
      InsertNode(p->right, data, mid + 1, high);
    }

   public:
    BinarySearchTree(const std::vector<int>& data) {
      if (!std::is_sorted(data.cbegin(), data.cend())) {
        throw std::invalid_argument("Input array isn't sorted.");
      }
      InsertNode(root, data, 0, data.size());
    }

    bool Contains(int x) {
      Node* p = root.get();
      for (; p && p->value != x;) {
        p = p->value > x ? p->left.get() : p->right.get();
      }
      return p != nullptr;
    }
  };

  TEST_CASE("Simple BinarySearchTree from sorted array.") {
    BinarySearchTree t{{3, 5, 6, 23}};
    CHECK(t.Contains(6));
    CHECK(t.Contains(3));
    CHECK(t.Contains(5));
    CHECK(t.Contains(23));
  }

  TEST_CASE("Simple BinarySearchTree from sorted array with repeating values.") {
    BinarySearchTree t{{3, 5, 6, 6, 23}};
    CHECK(t.Contains(6));
    CHECK(t.Contains(3));
    CHECK(t.Contains(5));
    CHECK(t.Contains(23));
  }
}

