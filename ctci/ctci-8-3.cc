/*
 * Haribol!
 * Problem: ctci 8.3
 */

#include <cstddef>
#include <vector>
#include <type_traits>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  using Index = std::vector<int>::size_type;
  constexpr auto kMaxIndex = static_cast<Index>(-1);

  Index BinarySearch(const std::vector<int>& a, const int key, const Index low, const Index high) {
    if (low == high) {
      return low;
    }

    auto mid = (low + high) / 2;

    if (a[mid] == key) {
      return mid;
    }

    if (a[mid] < key) {
      return BinarySearch(a, key, mid + 1, high);
    }

    return BinarySearch(a, key, low, mid);
  }

  Index MagicSearch(const std::vector<int>& a, const Index low, const Index high) {
    if (low == high) {
      return kMaxIndex;
    }

    auto mid = (low + high) / 2;

    if (a[mid] == mid) {
      return mid;
    }

    if (a[mid] < mid) {
      return MagicSearch(a, mid + 1, high);
    }

    return MagicSearch(a, low, mid);
  }

  Index MagicSearch2(const std::vector<int>& a, const Index low, const Index high) {
    if (low == high) {
      return kMaxIndex;
    }

    const auto mid = (low + high) / 2;

    if (a[mid] == mid) {
      return mid;
    }

    const auto leftChild = (low + mid) / 2;
    const auto rightChild = (mid + 1 + high) / 2;

    if (a[mid] < mid) {
      if (mid > low)
      Index i = mid - 1;
      while (i > low && a[i] == a[mid]) {
        if (a[i] == i) {
          return i;
        }
      }
      return MagicSearch(a, mid + 1, high);
    }

    return MagicSearch(a, low, mid);
  }

  Index MagicIndex(const std::vector<int>& a) {
    using Size = std::remove_reference_t<decltype(a)>::size_type;
    for (Size i = 0; i < a.size(); ++i) {
      auto ix = BinarySearch(a, static_cast<int>(i), i, a.size());
      if (ix >= a.size()) {
        break; // magic index not present
      }
      if (a[ix] == static_cast<int>(i) && ix == i) {
        return i;
      }
    }
    return kMaxIndex;
  }

  TEST_CASE("") {
    std::vector<int> a{3, 4, 5};
    CHECK(MagicIndex(a) == kMaxIndex);
    CHECK(MagicSearch(a, 0, a.size()) == kMaxIndex);
  }

  TEST_CASE("") {
    std::vector<int> a{-3, 0, 1, 3, 5};
    CHECK(MagicIndex(a) == 3);
    CHECK(MagicSearch(a, 0, a.size()) == 3);
  }
}

