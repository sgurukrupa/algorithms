/*
 * Haribol!
 * Problem: Implement a function to check if a binary tree is balanced. For the purposes of this question, a balanced tree is defined to be a tree such that the heights of the two subtrees of any node never differ by more than one
 */

#include <memory>
#include <cstddef>
#include <algorithm>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  struct Node {
    int value;
    std::shared_ptr<Node> left, right;
    Node(int x) : value(x) {}
  };

  inline std::shared_ptr<Node> MN(int x) {
    return std::make_shared<Node>(x);
  }

  inline std::shared_ptr<Node> MN(std::shared_ptr<Node>&& left, int x) {
    auto n = MN(x);
    n->left = std::move(left);
    return n;
  }

  inline std::shared_ptr<Node> MN(int x, std::shared_ptr<Node>&& right) {
    auto n = MN(x);
    n->right = std::move(right);
    return n;
  }

  inline std::shared_ptr<Node> MN(std::shared_ptr<Node>&& left, int x, std::shared_ptr<Node>&& right) {
    auto n = MN(x);
    n->left = std::move(left);
    n->right = std::move(right);
    return n;
  }

  inline std::shared_ptr<Node> MN(int xf, int x, std::shared_ptr<Node>&& right) {
    auto n = MN(x);
    n->left = MN(xf);
    n->right = std::move(right);
    return n;
  }

  inline std::shared_ptr<Node> MN(std::shared_ptr<Node>&& left, int x, int xr) {
    auto n = MN(x);
    n->left = std::move(left);
    n->right = MN(xr);
    return n;
  }

  inline std::shared_ptr<Node> MN(int xf, int x, int xr) {
    auto n = MN(x);
    n->left = MN(xf);
    n->right = MN(xr);
    return n;
  }

  class BinaryTree {
   private:
    std::shared_ptr<Node> root_;

    std::size_t GetHeight(const std::shared_ptr<Node>& n, bool& balanced) const {
      std::size_t hf = 0;
      if (n->left) {
        hf = GetHeight(n->left, balanced) + 1;
      }
      std::size_t hr = 0;
      if (n->right) {
        hr = GetHeight(n->right, balanced) + 1;
      }
      const auto mh = std::max(hf, hr);
      balanced = balanced && std::max(mh - hf, mh - hr) <= 1; // funny way of calculating abs diff, Haribol
      return mh;
    }

   public:
    BinaryTree(std::shared_ptr<Node>&& rt) : root_{std::move(rt)} {}

    bool IsBalanced() {
      auto balanced = true;
      GetHeight(root_, balanced);
      return balanced;
    }
  };

  TEST_CASE("BinaryTree isn't balanced.") {
    BinaryTree t{
      MN(MN(1, 2, 5), 2, MN(nullptr, 2, MN(4, 5, nullptr)))
    };
    CHECK(!t.IsBalanced());
  }

  TEST_CASE("BinaryTree is balanced.") {
    BinaryTree t{
      MN(MN(1, 2, 5), 2, MN(6, 2, MN(4, 5, 8)))
    };
    CHECK(t.IsBalanced());
  }
}
