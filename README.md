Prerequisites for building and running the code here: a C++ 17 or above compiler. I generally use Clang.
Hence, to build the code and run, cd into one of the folders and run the following command:

`clang++ quicksort.cc -I../inc -std=c++17 -Weverything -Wno-c++98-compat`

