#include <memory>
#include <algorithm>
#include <iterator>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  template <typename K, typename V>
  class BinarySearchTree {
   public:
    using key_type = K;
    using value_type = V;

   private:
    struct Node {
      const key_type key;
      value_type value;
      std::unique_ptr<Node> left, right;

      Node(key_type k, value_type v)
        : key{k}, value{v}
      {}

      std::vector<int> Inorder() {
        auto a = left ? left->Inorder() : std::vector<int>{};
        a.push_back(key);
        auto b = right ? right->Inorder() : std::vector<int>();
        std::copy(b.begin(), b.end(), std::back_inserter(a));
        return a;
      }
    };

    std::unique_ptr<Node> root_;

    std::unique_ptr<Node> DeleteMin(std::unique_ptr<Node>* i) {
      while ((*i)->left) { // seek out min
        i = &(*i)->left;
      }

      auto min = std::move(*i);
      *i = std::move(min->right);
      return min;
    }

   public:
    using key_type = K;
    using value_type = V;

    void Insert(const key_type key, const value_type& value) {
      std::unique_ptr<Node>* i = &root_;
      while (*i) {
        if ((*i)->key == key) { // found key - update value
          (*i)->value = value;
          return;
        }

        i = (key < (*i)->key) ? &(*i)->left : &(*i)->right;
      }

      *i = std::make_unique<Node>(key, value);
    }

    Node* Find(Node* i, key_type needle) {
      while (i && i->key != needle) {
        i = (needle < i->key) ? i->left.get() : i->right.get();
      }
      return i;
    }

    Node* Find(key_type needle) {
      return Find(root_.get(), needle);
    }

    Node* Min(Node* i) {
      Node* p = i;
      while (i) {
        p = i;
        i = i->left.get();
      }
      return p;
    }

    Node* Max(Node* i) {
      Node* p = i;
      while (i) {
        p = i;
        i = i->right.get();
      }
      return p;
    }

    // Node* argument is needed for recursion
    // Can't avoid recursion, Haribol!
    Node* Ceiling(Node* i, key_type key) {
      if (!i) {
        return i;
      }

      if (key < i->key) {
        auto j = Ceiling(i->left.get(), key);
        return !j ? i : j;
      }

      if (key > i->key) {
        return Ceiling(i->right.get(), key);
      }

      return i;
    }

    // Node* argument is needed for recursion
    // Can't avoid recursion, Haribol!
    //Node* Ceiling(Node* i, key_type key) {
    //  auto p = i;
    //  while (i && key != i->key) {
    //    while (i && key < i->key) {
    //      p = i;
    //      i = i->left.get();
    //    }
    //    while (i && key > i->key) {
    //      i = i->right.get();
    //    }
    //  }

    //  return p;
    //}

    // Node* argument is needed for recursion
    // Can't avoid recursion, Haribol!
    Node* Successor(Node* i, key_type key) {
      if (!i) {
        return i;
      }

      if (key < i->key) {
        auto j = Successor(i->left.get(), key);
        return !j ? i : j;
      }

      return Successor(i->right.get(), key);
    }

    // Node* argument is needed for recursion
    //Node* Successor(Node* i, key_type key) {
    //  auto p = i;
    //  while (i) {
    //    while (i && key < i->key) {
    //      p = i;
    //      i = i->left.get();
    //    }
    //    while (i && key >= i->key) {
    //      i = i->right.get();
    //    }
    //  }
    //  return p;
    //}

    Node* Successor(key_type key) {
      return Successor(root_.get(), key);
    }

    void Delete(const key_type key) {
      auto i = &root_;
      while (*i) {
        if (key < (*i)->key) {
          i = &(*i)->left;
        } else if (key > (*i)->key) {
          i = &(*i)->right;
        } else { // found key
          if (!(*i)->right) {
            *i = std::move((*i)->left);
          } else if (!(*i)->left) {
            *i = std::move((*i)->right);
          } else {
            auto min = DeleteMin(&(*i)->right);
            min->left = std::move((*i)->left);
            min->right = std::move((*i)->right);
            *i = std::move(min);
          }
          return;
        }
      }
    }

    std::vector<int> Inorder() {
      return root_ ? root_->Inorder() : std::vector<int>{};
    }
  };

  TEST_CASE("Insert a node in BST and search for it.") {
    BinarySearchTree<int, int> t;
    t.Insert(3, 4);
    CHECK(t.Find(3) != nullptr);
  }

  TEST_CASE("Insert multiple nodes in BST and search for smallest value.") {
    BinarySearchTree<int, int> t;
    t.Insert(3, 4);
    t.Insert(4, 4);
    t.Insert(5, 4);
    CHECK(t.Find(3) != nullptr);
  }

  TEST_CASE("Insert multiple nodes in BST and search for largest value.") {
    BinarySearchTree<int, int> t;
    t.Insert(3, 4);
    t.Insert(4, 4);
    t.Insert(5, 4);
    CHECK(t.Find(5) != nullptr);
  }

  TEST_CASE("Insert multiple nodes in BST and search for middle value.") {
    BinarySearchTree<int, int> t;
    t.Insert(3, 4);
    t.Insert(4, 4);
    t.Insert(5, 4);
    CHECK(t.Find(4) != nullptr);
  }

  TEST_CASE("Insert a node in BST and delete it.") {
    BinarySearchTree<int, int> t;
    t.Insert(3, 4);
    t.Delete(3);
    CHECK(t.Find(3) == nullptr);
  }

  TEST_CASE("Insert multiple nodes in BST; delete the smallest valued node.") {
    BinarySearchTree<int, int> t;
    t.Insert(3, 4);
    t.Insert(4, 4);
    t.Insert(5, 4);
    t.Delete(3);
    CHECK(t.Find(3) == nullptr);
  }

  TEST_CASE("Insert multiple nodes in BST; delete the largest valued node.") {
    BinarySearchTree<int, int> t;
    t.Insert(3, 4);
    t.Insert(4, 4);
    t.Insert(5, 4);
    t.Delete(5);
    CHECK(t.Find(5) == nullptr);
  }

  TEST_CASE("Insert multiple nodes in BST; delete the middle valued node.") {
    BinarySearchTree<int, int> t;
    t.Insert(3, 4);
    t.Insert(4, 4);
    t.Insert(5, 4);
    t.Delete(4);
    CHECK(t.Find(4) == nullptr);
  }

  //TEST_CASE("Insert multiple nodes in BST in ascending order and check if binary-search-tree property is maintained.") {
  //  BinarySearchTree t;
  //  t.Insert(3);
  //  t.Insert(4);
  //  t.Insert(5);
  //  auto a = t.Inorder();

  //  CHECK(std::is_sorted(a.begin(), a.end()));
  //}

  //TEST_CASE("Insert multiple nodes in BST in descending order and check if binary-search-tree property is maintained.") {
  //  BinarySearchTree t;
  //  t.Insert(5);
  //  t.Insert(4);
  //  t.Insert(3);
  //  auto a = t.Inorder();

  //  CHECK(std::is_sorted(a.begin(), a.end()));
  //}

  TEST_CASE("Insert multiple nodes in BST in random order and check if binary-search-tree property is maintained.") {
    BinarySearchTree<int, int> t;
    t.Insert(2, 4);
    t.Insert(5, 4);
    t.Insert(6, 4);
    t.Insert(4, 4);
    t.Insert(3, 4);
    t.Insert(8, 4);
    auto p = t.Successor(3);
    CHECK(p != nullptr);
    CHECK(p->key == 4);
  }

  //TEST_CASE("Insert multiple nodes in BST in random order and check if binary-search-tree property is maintained.") {
  //  BinarySearchTree t;
  //  t.Insert(2);
  //  t.Insert(5);
  //  t.Insert(6);
  //  t.Insert(4);
  //  t.Insert(3);
  //  t.Insert(8);
  //  auto a = t.Inorder();

  //  CHECK(std::is_sorted(a.begin(), a.end()));
  //}
}

