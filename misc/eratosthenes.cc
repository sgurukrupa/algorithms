/*
 * Haribol!
 * Sieve of Eratosthenes for generating prime numbers
 */

#include <vector>
#include <iostream>

namespace {
  std::vector<unsigned> Primes(unsigned n) {
    std::vector<bool> primes(n + 1, true);
    for (unsigned i = 2; i < n + 1; ++i) {
      for (unsigned m = 2; i * m < n + 1; ++m) {
        primes[i * m] = false;
      }
    }
    std::vector<unsigned> pv;
    for (decltype(primes)::size_type i = 2; i < primes.size(); ++i) {
      if (primes[i]) {
        pv.push_back(static_cast<unsigned>(i));
      }
    }
    return pv;
  }
}

int main() {
  for (auto e : Primes(500)) {
    std::cout << e << std::endl;
  }
}
