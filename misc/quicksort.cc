#include <iostream>
#include <vector>
#include <cstddef>
#include <utility>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  /*
   * Haribol!
   *   Design and implementation of the partition method is the trickiest in quicksort.
   * I found many different ways this method can be designed and implemented. Essentially
   * these differ in where we chose the pivot element. The pivot element is defined as the
   * one that partitions the array into two halves. The left half consist of all the elements
   * that are smaller than the pivot whereas the right half consist of those that are larger.
   * The pivot element can be chosen using some simple logic, e.g., it can be the start
   * element of the array. The most difficult to reason about design was the one where the
   * middle element is chosen as the pivot.
   *
   *   All the designs involve using two indices. In the designs where either the start
   * or the end elements of the array are chosen as the pivot element, one of the indices
   * scan through the array for elements that are smaller or larger than the pivot and the
   * other gathers those elements at one end of the array. I call these the 'scanner' and
   * 'gatherer' respectively. In the final step the pivot element is swapped with the
   * element that happens to be at the index 'following' the gatherer index. Note the
   * 'following' term should be understood in the sense of how the gatherer index is
   * progressing. If the gatherer index is decreasing, the 'following' index is one
   * less than the gatherer else it means one more than the latter.
   *
   *   I have shown four implementations of the Partition method below. In the first
   * version, the pivot is at the start of the array, whereas in the second version
   * it is at the end. I always start the scanner index (i) at the start of the array
   * and proceed toward the end. This avoids the problem of comparing the scanner
   * against the value -1 since the index is an unsigned type (size_t). Now, the
   * scanner shouldn't be crossing the pivot. The reason for this will be evident
   * at the final step. The remaining two versions are similar to these versions.
   *
   *   In the first version of Partition, as the scanner comes across elements
   * larger than the pivot, those elements are gathered at the end of the array.
   * Since the scanner is progressing in a direction opposite the gatherer, the
   * loop terminates when the scanner meets the gatherer. Also, since the gatherer
   * gathers elements by swapping with the elements that are being scanned by the
   * scanner, we must be careful not to allow any larger than pivot elements to
   * get past the scanner. Hence, we must not increment the scanner when it comes
   * across a larger-than-pivot element. Rather, the newly swapped element must
   * be tested against the pivot and swapped with the element one position before
   * the gatherer. The final step is to swap the pivot with the element at one
   * position before the gatherer which must be smaller than the pivot.
   *
   *   In the second version, the pivot element is at the end of the array. The
   * gatherer gathers elements at the start of the array. Both the scanner and
   * the gather are working in the same direction. The scanner starts at the same
   * position as the gatherer, but goes somewhat ahead of it later. This is because
   * its work is to scan for smaller-than-pivot elements and pass them on to the
   * gatherer. The considerations for the scanner here are different than those
   * in the first version. The scanner must go till one position before the end,
   * so that all smaller-than-pivot elements can be gathered at the start of the
   * array. The final step is the swap of the pivot with the element past the
   * gatherer.
   *
   *   Note that the swap in the final steps of both implmentations will maintain
   * the invariant which is "smaller-than-pivot elements before the pivot and
   * larger-than-pivot elmements after the pivot."
   */

  // 1st version of Partition
  // The pivot chosen is the one at the start of the array
  std::size_t Partition(std::vector<unsigned>& array,
      const std::size_t low,
      const std::size_t high) {

    auto pivot = array[low];
    auto i = low + 1, j = high;

    for (; i < j;) {
      if (array[i] > pivot) {
        std::swap(array[i], array[--j]); // puts the bigger-than-pivot element at the end
      } else {
        ++i;
      }
    }

    std::swap(array[j - 1], array[low]);
    return j - 1;
  }

  // 2nd version of Partition
  // The pivot chosen is the one at the end of the array
  std::size_t Partition2(std::vector<unsigned>& array,
      const std::size_t low,
      const std::size_t high) {

    auto pivot = array[high - 1];
    auto i = low, j = low;

    for (; i < high - 1; ++i) {
      if (array[i] < pivot) {
        // put the smaller-than-pivot element at the beginning
        // j points to the place where that element can be put
        // i.e. j may point to a bigger-than-pivot element that is available for swapping
        // with a smaller-than-pivot element pointed to by i.
        std::swap(array[i], array[j++]);
      }
    }

    std::swap(array[j], array[high - 1]);
    return j;
  }

  std::size_t Partition3(std::vector<unsigned>& array,
      const std::size_t low,
      const std::size_t high) {

    auto pivot = array[high - 1];
    auto i = low, j = high - 1;

    for (; i < j; ) {
      if (array[i] > pivot) {
        // put the larger-than-pivot element at the end
        // j is the starting index of all larger-than-pivot elements
        // i.e. j may point to a bigger-than-pivot element that is available for swapping
        // with a smaller-than-pivot element pointed to by i.
        std::swap(array[i], array[--j]);
      } else {
        ++i;
      }
    }

    std::swap(array[j], array[high - 1]);
    return j;
  }

  std::size_t Partition4(std::vector<unsigned>& array,
      const std::size_t low,
      const std::size_t high) {

    auto pivot = array[low];
    auto i = low + 1, j = low + 1;

    for (; i < high; ++i) {
      if (array[i] < pivot) {
        // put the smaller-than-pivot element at the beginning
        // j points to the place where that element can be put
        // i.e. j may point to a bigger-than-pivot element that is available for swapping
        // with a smaller-than-pivot element pointed to by i.
        std::swap(array[i], array[j++]);
      }
    }

    std::swap(array[j - 1], array[low]);
    return j - 1;
  }

  std::size_t Partition3Way(std::vector<unsigned>& array,
      const std::size_t low,
      const std::size_t high) {

    auto pivot = array[high - 1];
    auto i = low, j = high - 1, k = high - 1;

    for (; i < j; ) {
      if (array[i] > pivot) {
        // put the larger-than-pivot element at the end
        // j is the starting index of all larger-than-pivot elements
        // i.e. j may point to a bigger-than-pivot element that is available for swapping
        // with a smaller-than-pivot element pointed to by i.
        if (k == j) {
          std::swap(array[i], array[--k]);
          --j;
        } else {
          std::swap(array[i], array[--j]);
          std::swap(array[j], array[--k]);
        }
      } else if (array[i] == pivot) {
        std::swap(array[i], array[--j]);
      } else {
        ++i;
      }
    }

    std::swap(array[k], array[high - 1]);
    return (j + k) / 2;
  }

  void QuickSort(std::vector<unsigned>& array, std::size_t low, std::size_t high) {
    if (low == high) {
      return;
    }

    auto mid = Partition3Way(array, low, high);
    QuickSort(array, low, mid);
    QuickSort(array, mid + 1, high);
  }

  TEST_CASE("Quicksort array with even number of non-distinct elements") {
    std::vector<unsigned> a = { 2, 3, 2, 6, 3, 1, 4, 5 };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with even number of distinct elements") {
    std::vector<unsigned> a = { 2, 3, 8, 6, 11, 1, 4, 5 };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with odd number of non-distinct elements") {
    std::vector<unsigned> a = { 2, 3, 2, 3, 1, 4, 5 };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with odd number of distinct elements") {
    std::vector<unsigned> a = { 2, 3, 6, 11, 1, 4, 5 };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with odd number of equal elements") {
    std::vector<unsigned> a = { 2, 2, 2, 2, 2 };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with even number of equal elements") {
    std::vector<unsigned> a = { 2, 2, 2, 2 };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with even number of equal elements") {
    std::vector<unsigned> a = { 2, 2, 2, 2 };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with two non-distinct elements") {
    std::vector<unsigned> a = { 2, 2, };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with two distinct elements") {
    std::vector<unsigned> a = { 4, 2, };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with two distinct elements already sorted in increasing order") {
    std::vector<unsigned> a = { 2, 4, };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with distinct elements already sorted in increasing order") {
    std::vector<unsigned> a = { 2, 4, 7, 8, 9 };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with non-distinct elements already sorted in increasing order") {
    std::vector<unsigned> a = { 2, 4, 4, 8, 9 };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with distinct elements sorted in decreasing order") {
    std::vector<unsigned> a = { 5, 4, 3, 2, 1 };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }

  TEST_CASE("Quicksort array with non-distinct elements sorted in decreasing order") {
    std::vector<unsigned> a = { 5, 5, 3, 3, 1 };
    QuickSort(a, 0, a.size());
    for (decltype(a)::size_type i = 1; i < a.size(); ++i) {
      CHECK(a[i - 1] <= a[i]);
    }
  }
}

/*
int main() {
  std::vector<unsigned> a = { 2, 3, 2, 6, 3, 1, 4, 5 };
  //std::vector<unsigned> a = { 1, 2, 3, 4, 5 };
  //std::vector<unsigned> a = { 5, 4, 3, 2, 1 };
  //std::vector<unsigned> a = { 5, 2, 2, 4 };
  std::cout << "Before sorting: ";
  Print(a);
  std::cout << std::endl;
  QuickSort(a, 0, a.size());
  std::cout << "After sorting: ";
  Print(a);
  std::cout << std::endl;
}
*/

