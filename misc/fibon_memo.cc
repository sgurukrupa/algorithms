/*
 * Haribol!
 * Jaya Srila Prabhupada!
 * A fibonacci series production problem solved using dynamic programming / memoization
 */

#include <stdexcept>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  unsigned fibonacci(int n) {
    if (n == 0 || n == 1) {
      return n;
    }

    unsigned f0 = 0;
    unsigned f1 = 1;
    unsigned sum;
    for (int i = 2; i <= n; ++i) {
      sum = f0 + f1;

      if (sum < f0 || sum < f1) {
        throw std::overflow_error("fibonacci overflow - Hare Krishna!");
      }

      f0 = f1, f1 = sum;
    }
    return sum;
  }

  TEST_CASE("fibonacci(2).") {
    CHECK(fibonacci(2) == 1);
  }

  TEST_CASE("fibonacci(3).") {
    CHECK(fibonacci(3) == 2);
  }

  TEST_CASE("fibonacci(4).") {
    CHECK(fibonacci(4) == 3);
  }

  TEST_CASE("fibonacci(5).") {
    CHECK(fibonacci(5) == 5);
  }

  TEST_CASE("fibonacci(6).") {
    CHECK(fibonacci(6) == 8);
  }

  TEST_CASE("fibonacci(10).") {
    CHECK(fibonacci(10) == 8);
  }
}

