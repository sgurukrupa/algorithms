#include <algorithm>
#include <string>
#include <stdexcept>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  using Costs = std::unordered_map<std::string, unsigned>;
  using Parents = std::unordered_map<std::string, std::string>;
  using Neighbors = std::vector<std::pair<std::string, unsigned>>;

  std::string FindLeastCostNode(const Costs& costs,
      const std::unordered_set<std::string>& visited) {
    std::vector<std::pair<std::string, unsigned>> sc{costs.cbegin(),
      costs.cend()};
    std::sort(sc.begin(), sc.end(), [](
          const std::pair<std::string, unsigned>& nw1,
          const std::pair<std::string, unsigned>& nw2) {
          return nw1.second < nw2.second;
        });
    //got this idea by Lord Krishna's mercy, Haribol
    for (auto& p : sc) {
      if (!visited.count(p.first)) {
        return p.first;
      }
    }
    throw std::domain_error("Unable to reach target!");
  }

  void ComputeDijsktra(const std::unordered_map<std::string, Neighbors>& graph,
      const std::string& src, const std::string& tgt, Costs& costs,
      Parents& parents) {
    std::unordered_set<std::string> visited;
    costs[src] = 0; // cost of the source node is zero
    std::string lcn{src}; // lcn => least cost node
    while (lcn != tgt) {
      visited.insert(lcn);
      auto& ngbrs = graph.at(lcn); // ngbrs => neighbors
      auto cost = costs[lcn]; // cost of lcn
      for (auto& n : ngbrs) {
        auto ci = costs.find(n.first);
        if (ci == costs.end() || ci->second > cost + n.second) {
          costs.insert_or_assign(ci, n.first, cost + n.second);
          parents[n.first] = lcn;
        }
      }
      lcn = FindLeastCostNode(costs, visited);
    }
  }

  inline std::pair<std::string, unsigned> N(const std::string& vertex, const unsigned weight) {
    return std::make_pair(vertex, weight);
  }

  inline std::pair<std::string, Neighbors> L(const std::string& vertex, Neighbors ngbrs) {
    return std::make_pair(vertex, std::move(ngbrs));
  }

  TEST_CASE("Simple graph.") {
    Costs c;
    Parents p;
    ComputeDijsktra({
        L("S", {N("A", 6), N("B", 2)}),
        L("A", {N("F", 1)}),
        L("B", {N("A", 3), N("F", 5)}),
        }, "S", "F", c, p);
    CHECK(c["F"] == 6);
    CHECK(p["F"] == "A");
    CHECK(p["A"] == "B");
    CHECK(p["B"] == "S");
  }

  TEST_CASE("Complex graph.") {
    Costs c;
    Parents p;
    ComputeDijsktra({
        L("A", {N("B", 5), N("D", 2)}),
        L("B", {N("C", 4), N("E", 2)}),
        L("D", {N("B", 8), N("E", 7)}),
        L("C", {N("E", 6), N("F", 3)}),
        L("E", {N("F", 1)}),
        }, "A", "F", c, p);
    CHECK(c["F"] == 8);
    CHECK(p["F"] == "E");
    CHECK(p["E"] == "B");
    CHECK(p["B"] == "A");
  }

  TEST_CASE("Graph with positive weight cycle.") {
    Costs c;
    Parents p;
    ComputeDijsktra({
        L("A", {N("B", 10)}),
        L("B", {N("C", 20)}),
        L("C", {N("E", 1), N("D", 30)}),
        L("E", {N("B", 1)}),
        }, "A", "D", c, p);
    CHECK(c["D"] == 60);
    CHECK(p["D"] == "C");
    CHECK(p["C"] == "B");
    CHECK(p["B"] == "A");
  }
}

