#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  TEST_CASE("Larger signed type when assigned to smaller signed type gets stored as the same value modulo 2^n where n is sizeof(smaller type)") {
    static_assert(sizeof(int) > sizeof(short), "int type should be bigger than the short type for this test to work.");

    int a = 0x1ffff;
    short b = a;
    CHECK(static_cast<unsigned short>(b) == 0xffff);
    a = 0x3abcd;
    b = a;
    CHECK(static_cast<unsigned short>(b) == 0xabcd);
    a = -0x3abcd;
    b = a;
    CHECK(static_cast<unsigned short>(b) == static_cast<unsigned short>(1 + ~0xabcd));
  }
}
