#include <algorithm>
#include <vector>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

namespace {
  class MinHeap {
   private:
    std::vector<int> data_;

   public:
    using Index = decltype(data_)::size_type;

    MinHeap() : heapsize_(0) {}

    MinHeap(const std::initializer_list<int>& t)
      : data_(t) {
        Heapify();
      }

    MinHeap& Insert(const int x) {
      data_.push_back(x);
      PushUp(heapsize_++, x);
      return *this;
    }

    int Pop() {
      if (heapsize_ == 0) {
        throw std::out_of_range("Heap size is zero.");
      }

      auto v = data_[0];
      data_[0] = data_[--heapsize_];
      Reheap(0);
      return v;
    }

   private:
    Index heapsize_;

    void Reheap(const Index i) {
      // indices of left and right children, Haribol
      auto lfix = 2*i + 1, rtix = 2*i + 2;
      auto smix = i;
      if (lfix < heapsize_ && data_[lfix] < data_[smix]) {
        smix = lfix;
      }
      if (rtix < heapsize_ && data_[rtix] < data_[smix]) {
        smix = rtix;
      }

      if (smix != i) {
        std::swap(data_[smix], data_[i]);
        Reheap(smix); // recursively reheap child node, Haribol
      }
    }

    void PushUp(Index i, const int x) {
      if (data_[i] < x) {
        throw std::invalid_argument("new value is greater than existing value.");
      }

      auto pix = i; // parent's index

      do {
        data_[i] = data_[pix]; // unnecessary copy in first iteration
        i = pix;
        pix = (i - 1) / 2; // overflows if i == 0
      } while (i > 0 && data_[pix] > x);

      data_[i] = x;
    }

    void Heapify() {
      heapsize_ = data_.size();
      // Reheap all non-leaf nodes
      // start from the last non-leaf node - index: (heapsize_ / 2) - 1
      for (auto i = heapsize_ / 2; i > 0; --i) {
        Reheap(i - 1);
      }
    }
  };

  TEST_CASE("Insert a value in MinHeap and pop it.") {
    MinHeap t;
    t.Insert(3);
    CHECK(t.Pop() == 3);
  }

  TEST_CASE("Construct MinHeap with a list of values and pop them - should be sorted.") {
    MinHeap t { 4, 3, 11, 2 };
    CHECK(t.Pop() == 2);
    CHECK(t.Pop() == 3);
    CHECK(t.Pop() == 4);
    CHECK(t.Pop() == 11);
  }

  TEST_CASE("Insert multiple values in MinHeap and pop them - should be sorted.") {
    MinHeap t;
    t.Insert(4).Insert(3).Insert(11).Insert(2);
    CHECK(t.Pop() == 2);
    CHECK(t.Pop() == 3);
    CHECK(t.Pop() == 4);
    CHECK(t.Pop() == 11);
  }
}

